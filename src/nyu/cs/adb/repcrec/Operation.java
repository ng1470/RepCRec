package nyu.cs.adb.repcrec;

/**
 * Class representing the required information of an Operation.
 * 
 * @author Nishma Gorwara
 */
public class Operation {

    OperationType type;
    int transactionId;
    int variable;
    int value;
    int site;

    /**
     * Constructor to set initial values for member variables of an Operation.
     * 
     */
    Operation() {

	type = null;
	transactionId = Integer.MIN_VALUE;
	variable = Integer.MIN_VALUE;
	value = Integer.MIN_VALUE;
	site = Integer.MIN_VALUE;

    }

}
