package nyu.cs.adb.repcrec;

import java.util.ArrayList;
import java.util.TreeMap;

/**
 * Class representing the required information for a Variable.
 * 
 * @author Jatin Malviya
 */
public class Variable {

    int variableId;
    Integer value = null;
    TreeMap<Integer, Integer> valueHistory = null;
    boolean isReplicated = false;
    boolean isStale = false;

    /**
     * Method to initialize object Variable.
     * 
     * @param id
     *            Id of the Variable.
     * @param initialValue
     *            Initialize value for the variable.
     * 
     * @author Jatin Malviya
     */
    Variable(int id, Integer initialValue) {

	variableId = id;
	value = initialValue;
	valueHistory = new TreeMap<Integer, Integer>();
	valueHistory.put(0, value);
	isStale = false;

	if (id % 2 == 0) {

	    // Since Even indexed variables are at all sites.
	    isReplicated = true;

	} else {

	    // Since Odd indexed variables are at one site each.
	    isReplicated = false;

	}

    }

    /**
     * Method to update the value and history of the Variable when Write
     * operation happens for a Transaction.
     * 
     * @param val
     *            Update value for the Variable during Write operation.
     * @param timestamp
     *            Time at which value is updated during Write operation.
     * 
     * @author Jatin Malviya
     */
    public void writeVariable(int val, int timestamp) {

	valueHistory.put(timestamp, val);
	value = val;

    }

    /**
     * Method to get current value of a Variable.
     * 
     * @return Current value returned of a Variable.
     * 
     * @author Jatin Malviya
     */
    public Integer getValue() {
	return value;
    }

    /**
     * Method to get oldest value of a Variable before a given timestamp.
     * 
     * @return Oldest value returned of a Variable.
     * 
     * @author Jatin Malviya
     */
    public Integer getValue(int timestamp) {

	ArrayList<Integer> TS = new ArrayList<Integer>(valueHistory.keySet());

	for (int i = TS.size() - 1; i >= 0; i--) {

	    // Return oldest value of Variable before the given timestamp
	    if (TS.get(i) <= timestamp) {
		return valueHistory.get(TS.get(i));
	    }

	}

	// Return initial value of Variable if no value found before timestamp
	return Integer.MIN_VALUE;

    }

}
