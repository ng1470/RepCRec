package nyu.cs.adb.repcrec;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Class representing the required information for a Transaction.
 * 
 * @author Nishma Gorwara
 */
public class Transaction {

    int transactionId;
    int startTime;
    TransactionStatus status;
    Operation currentOperation;
    boolean isReadOnly;

    ArrayList<Integer> waitingForTransactions = new ArrayList<Integer>();
    HashMap<Integer, Integer> valuesRead = new HashMap<Integer, Integer>();
    HashMap<Integer, Integer> valuesWritten = new HashMap<Integer, Integer>();

    HashMap<Site, ArrayList<Integer>> siteAcessTime = new HashMap<Site, ArrayList<Integer>>();

    HashMap<Integer, Integer> readlocks = new HashMap<Integer, Integer>();
    HashMap<Integer, ArrayList<Integer>> writelocks = new HashMap<Integer, ArrayList<Integer>>();
    
    //For ReadOnly
    HashSet<Integer> activeStartSites = new HashSet<Integer>();

    /**
     * Method to Abort the Transaction by setting the required member variables.
     * 
     * @author Nishma Gorwara
     */
    void abort() {

	status = TransactionStatus.Aborted;
	currentOperation = null;
	waitingForTransactions = null;
	valuesRead = null;
	valuesWritten = null;
	siteAcessTime = null;
	readlocks = null;
	writelocks = null;
	activeStartSites = null;
    }

    /**
     * Method to Commit the Transaction by setting the required member
     * variables.
     * 
     * @author Nishma Gorwara
     */
    void commit() {

	status = TransactionStatus.Completed;
	siteAcessTime = null;
	readlocks = null;
	writelocks = null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString(java.lang.Object)
     */
    @Override
    public String toString() {
	return "T" + transactionId;
    }

}
