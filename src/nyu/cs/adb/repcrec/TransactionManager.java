package nyu.cs.adb.repcrec;

import java.util.ArrayList;
import java.util.TreeMap;

/**
 * Class representing a Transaction Manager that translates read and write
 * requests on variables to read and write requests on copies using the
 * available copy algorithm. The transaction manager never fails.
 * 
 * @author Jatin Malviya
 * @author Nishma Gorwara
 */
public class TransactionManager {

    int currentTime;
    TreeMap<Integer, Transaction> transactions;
    ArrayList<Transaction> waitingTransactions;
    DatabaseManager databaseManager;

    /**
     * Constructor to initialize the member variables of Traansaction Manager.
     * 
     */
    TransactionManager() {

	currentTime = 0;
	transactions = new TreeMap<Integer, Transaction>();
	waitingTransactions = new ArrayList<Transaction>();
	databaseManager = new DatabaseManager();
	databaseManager.initialize();

    }

    /**
     * Method to process an Operation at given time and also take appropriate
     * action for the waiting transactions.
     * 
     * @param currOperation
     *            Current operation which is needed to be processed.
     * @param time
     *            Timestamp of the current Operation.
     * 
     * @author Nishma Gorwara
     */
    public void tick(Operation currOperation, int time) {

	currentTime = time;

	processOperation(currOperation);

	processWaitingTransactions();

	ArrayList<Transaction> isDeadlock = detectCycleWait();
	while (isDeadlock != null) {

		Transaction min = null;

	    for (Transaction tran : isDeadlock) {

		if (min == null || tran.startTime > min.startTime) {
		    min = tran;
		}

	    }

	    System.out.println("******Deadlock Found Between " + isDeadlock + " Aborting Younger Transaction " + min);
	    removeLocksTransaction(min);
	    min.abort();
	    System.out.println("Transaction T" + min.transactionId + " aborted to break Deadlock" + " at time " + currentTime);
	    waitingTransactions.remove(min);

	    processWaitingTransactions();

	    isDeadlock = detectCycleWait();
	    
	}

   }

    /**
     * Method to process a given Operation of a Transaction based on its type.
     * 
     * @param operation
     *            Given operation with its type of a Transaction.
     * 
     * @author Nishma Gorwara
     */
    public void processOperation(Operation operation) {

	if (operation == null) {

	    throw new IllegalStateException("Null Operation Type passed.");
	}

	switch (operation.type) {

	case Begin:
	    beginTransaction(operation);
	    return;

	case BeginRO:
	    beginROTransaction(operation);
	    return;

	case Read:
	    readOperation(operation);
	    break;

	case Write:
	    writeOperation(operation);
	    break;

	case Fail:
	    failOperation(operation);
	    break;

	case Recover:
	    recoverOperation(operation);
	    break;

	case End:
	    endOperation(operation);
	    break;

	case Dump:
	    dumpOperation(operation);
	    break;

	case DumpI:
	    dumpIOperation(operation);
	    break;

	case DumpXj:
	    dumpXjOperation(operation);
	    break;

	default:
	    throw new IllegalStateException("Operation Type: " + operation.type + "not handled.");

	}

    }

    /**
     * Method to process list of waiting transactions by executing their
     * operation.
     * 
     * @author Nishma Gorwara
     */
    void processWaitingTransactions() {

	ArrayList<Transaction> lis = new ArrayList<Transaction>(waitingTransactions);

	for (Transaction t : lis) {
	    t.waitingForTransactions.clear();
	    processOperation(t.currentOperation);

	}

    }

    /**
     * Method to begin a Transaction when given an Operation.
     * 
     * @param operation
     *            Given operation with its transaction id.
     * 
     * @author Jatin Malviya
     */
    void beginTransaction(Operation operation) {

	if (operation.transactionId == Integer.MIN_VALUE) {

	    throw new IllegalStateException("Transaction ID missing in Begin Operation");
	}

	int id = operation.transactionId;

	if (transactions.containsKey(id)) {

	    throw new IllegalStateException("Trying to create duplicate Transaction T" + id);

	}

	Transaction transaction = new Transaction();
	transaction.transactionId = id;
	transaction.isReadOnly = false;
	transaction.currentOperation = null;
	transaction.startTime = currentTime;
	transaction.status = TransactionStatus.Active;

	transactions.put(id, transaction);

    }

    /**
     * Method to begin a Read-only Transaction when given an Operation.
     * 
     * @param operation
     *            Given operation with its read-only transaction id.
     * 
     * @author Jatin Malviya
     */
    void beginROTransaction(Operation operation) {

	if (operation.transactionId == Integer.MIN_VALUE) {

	    throw new IllegalStateException("Transaction ID missing in Begin Operation");
	}

	int id = operation.transactionId;

	if (transactions.containsKey(id)) {

	    throw new IllegalStateException("Trying to create duplicate Transaction T" + id);

	}

	Transaction transaction = new Transaction();
	transaction.transactionId = id;
	transaction.isReadOnly = true;
	transaction.currentOperation = null;
	transaction.startTime = currentTime;
	transaction.status = TransactionStatus.Active;
	transaction.waitingForTransactions.clear();
	
	for(Site s: databaseManager.getSites()) {
		if(s.status != SiteStatus.Failed) {
			transaction.activeStartSites.add(s.siteId);
		}
	}

	transactions.put(id, transaction);

    }

    /**
     * Method for Read Operation of a Transaction for the given Variable.
     * 
     * @param operation
     *            Operation with Read instruction for a Transaction.
     * 
     * @author Jatin Malviya
     */
    void readOperation(Operation operation) {

	if (operation.transactionId == Integer.MIN_VALUE) {
	    throw new IllegalStateException("Transaction ID missing in Read Operation");
	}

	if (operation.variable == Integer.MIN_VALUE) {
	    throw new IllegalStateException("Variable ID missing in Read Operation");
	}

	if (!transactions.containsKey(operation.transactionId)) {
	    throw new IllegalStateException("No Transaction with ID " + operation.transactionId);
	}

	Transaction transaction = transactions.get(operation.transactionId);

	// Check if the Transaction is in Waiting state.
	if (transaction.status == TransactionStatus.Waiting && transaction.currentOperation != operation) {

	    throw new IllegalStateException(
		    "Transaction T" + transaction.transactionId + " is in Waiting state, Cannot run new Operation.");

	}

	transaction.currentOperation = operation;

	int variableId = operation.variable;

	// Check if the Transaction has already read the Variable
	if (transaction.valuesRead.containsKey(variableId)) {

	    System.out.println("Transaction T" + transaction.transactionId + " has already read x" + variableId
		    + " with value " + transaction.valuesRead.get(variableId) + " in the past.");

	    if (waitingTransactions.contains(transaction)) {
		waitingTransactions.remove(transaction);
	    }

	    transaction.status = TransactionStatus.Active;
	    transaction.waitingForTransactions.clear();
	    transaction.currentOperation = null;
	    return;

	}

	// Check if the Transaction has already written the Variable
	if (transaction.valuesWritten.containsKey(variableId)) {

	    System.out.println("Transaction T" + transaction.transactionId + " is already going to change x"
		    + variableId + " with value " + transaction.valuesWritten.get(variableId));

	    if (waitingTransactions.contains(transaction)) {
		waitingTransactions.remove(transaction);
	    }

	    transaction.status = TransactionStatus.Active;
	    transaction.waitingForTransactions.clear();
	    transaction.currentOperation = null;
	    return;

	}

	// Check if the Transaction is read-only or normal
	if (transaction.isReadOnly) {
	    roReadOperation(transaction, variableId);
	} else {
	    normalReadOperation(transaction, variableId);
	}

    }

    /**
     * Method to read a Variable of a Read-only Transaction.
     * 
     * @param transaction
     *            Read-only Transaction for Read Operation.
     * @param vID
     *            Variable id whose value is read.
     * 
     * @author Jatin Malviya
     */
    void roReadOperation(Transaction transaction, int vID) {

	for (Site s : databaseManager.getSites()) {

	    if (s.canReadROVariable(transaction, vID)) {

		int value = s.readRO(transaction, vID, transaction.startTime);
		transaction.valuesRead.put(vID, value);
		
		if (transaction.siteAcessTime.containsKey(s)) {
		    ArrayList<Integer> temp = transaction.siteAcessTime.get(s);
		    temp.add(currentTime);
		    transaction.siteAcessTime.put(s, temp);
		} else {
		    ArrayList<Integer> temp = new ArrayList<Integer>();
		    temp.add(currentTime);
		    transaction.siteAcessTime.put(s, temp);
		}

		System.out.println("RO Transaction T" + transaction.transactionId + " reads variable x" + vID
			+ " with value " + value + " from Site " + s.siteId + " at time " + currentTime);

		if (waitingTransactions.contains(transaction)) {
		    waitingTransactions.remove(transaction);
		}
		transaction.status = TransactionStatus.Active;
		transaction.currentOperation = null;
		transaction.waitingForTransactions.clear();
		return;
	    }

	}

	transaction.status = TransactionStatus.Waiting;
	transaction.waitingForTransactions.clear();

	if (!waitingTransactions.contains(transaction)) {
	    waitingTransactions.add(transaction);
	}

    }

    /**
     * Method to read a Variable of a Normal Transaction.
     * 
     * @param transaction
     *            Normal Transaction for Read Operation.
     * @param vID
     *            Variable id whose value is read.
     * 
     * @author Jatin Malviya
     */
    void normalReadOperation(Transaction transaction, int vID) {

	for (Site s : databaseManager.getSites()) {

	    if (s.canReadVariable(transaction, vID)) {

		s.putReadLock(transaction, vID);
		transaction.readlocks.put(vID, s.siteId);

		if (transaction.siteAcessTime.containsKey(s)) {
		    ArrayList<Integer> temp = transaction.siteAcessTime.get(s);
		    temp.add(currentTime);
		    transaction.siteAcessTime.put(s, temp);
		} else {
		    ArrayList<Integer> temp = new ArrayList<Integer>();
		    temp.add(currentTime);
		    transaction.siteAcessTime.put(s, temp);
		}

		int value = s.read(transaction, vID);
		transaction.valuesRead.put(vID, value);

		System.out.println("Transaction T" + transaction.transactionId + " reads variable x" + vID
			+ " with value " + value + " from Site " + s.siteId + " at time " + currentTime);

		if (waitingTransactions.contains(transaction)) {
		    waitingTransactions.remove(transaction);
		}
		transaction.status = TransactionStatus.Active;
		transaction.waitingForTransactions.clear();
		transaction.currentOperation = null;
		return;

	    } else if (!s.canReadVariable(transaction, vID) && s.hasVariable(vID) && s.status != SiteStatus.Failed) {

		// The variable is write locked by another Transaction

		int blockedBy = s.variableWriteLockedBy(vID);

		transaction.status = TransactionStatus.Waiting;
		if (!transaction.waitingForTransactions.contains(blockedBy)) {
		    transaction.waitingForTransactions.add(blockedBy);
		}
		if (!waitingTransactions.contains(transaction)) {
		    waitingTransactions.add(transaction);
		}

	    }

	}

	// All Sites down
	if (!waitingTransactions.contains(transaction)) {

	    transaction.status = TransactionStatus.Waiting;
	    transaction.waitingForTransactions.clear();
	    waitingTransactions.add(transaction);
	}

    }

    /**
     * Method for Write Operation of a Transaction on the given Variable with
     * write value.
     * 
     * @param operation
     *            Operation with Write instruction for a Transaction.
     * 
     * @author Jatin Malviya
     */
    void writeOperation(Operation operation) {

	if (operation.transactionId == Integer.MIN_VALUE) {

	    throw new IllegalStateException("Transaction ID missing in Write Operation");
	}

	if (operation.variable == Integer.MIN_VALUE) {
	    throw new IllegalStateException("Variable ID missing in Write Operation");
	}

	if (operation.value == Integer.MIN_VALUE) {
	    throw new IllegalStateException("New Value missing in Write Operation");
	}

	if (!transactions.containsKey(operation.transactionId)) {
	    throw new IllegalStateException("No Transaction with ID " + operation.transactionId);
	}

	Transaction transaction = transactions.get(operation.transactionId);

	// Check if Transaction is in Waiting state
	if (transaction.status == TransactionStatus.Waiting && transaction.currentOperation != operation) {

	    throw new IllegalStateException(
		    "Transaction T" + transaction.transactionId + " is in Waiting state, Cannot run new Operation.");

	}

	transaction.currentOperation = operation;

	int variableId = operation.variable;
	int newValue = operation.value;

	// Check if Transaction has already written value of the variable then
	// overwrite it
	if (transaction.valuesWritten.containsKey(variableId) && transaction.status != TransactionStatus.Waiting) {

	    System.out.println("Transaction T" + transaction.transactionId + " has already changed x" + variableId
		    + " with value " + transaction.valuesWritten.get(variableId) + ". Overwriting it to" + newValue);

	    transaction.valuesWritten.put(variableId, newValue);

	    if (waitingTransactions.contains(transaction)) {
		waitingTransactions.remove(transaction);
	    }

	    transaction.status = TransactionStatus.Active;
	    transaction.waitingForTransactions.clear();
	    transaction.currentOperation = null;
	    return;

	}

	boolean allSitesAcquired = true;
	boolean shouldWait = true;

	transaction.valuesWritten.put(variableId, newValue);

	for (Site s : databaseManager.getSites()) {

	    if (s.hasVariable(variableId) && s.status != SiteStatus.Failed) {
		shouldWait = false;
		if (s.canWriteVariable(transaction, variableId)) {

		    s.putWriteLock(transaction, variableId);
		    ArrayList<Integer> newlocks;

		    if (transaction.writelocks.containsKey(variableId)) {
			newlocks = transaction.writelocks.get(variableId);
		    } else {
			newlocks = new ArrayList<Integer>();
		    }

		    if (!newlocks.contains(s.siteId)) {
			newlocks.add(s.siteId);
		    }

		    transaction.writelocks.put(variableId, newlocks);

		    if (transaction.siteAcessTime.containsKey(s)) {
			ArrayList<Integer> temp = transaction.siteAcessTime.get(s);
			temp.add(currentTime);
			transaction.siteAcessTime.put(s, temp);
		    } else {
			ArrayList<Integer> temp = new ArrayList<Integer>();
			temp.add(currentTime);
			transaction.siteAcessTime.put(s, temp);
		    }

		} else {
		    allSitesAcquired = false;

		    // The variable is write/read locked by another Transaction
		    if (s.variableIsWriteLocked(variableId)) {

			int blockedBy = s.variableWriteLockedBy(variableId);
			transaction.status = TransactionStatus.Waiting;
			if (!transaction.waitingForTransactions.contains(blockedBy)) {
			    transaction.waitingForTransactions.add(blockedBy);
			}
			if (!waitingTransactions.contains(transaction)) {
			    waitingTransactions.add(transaction);
			}
		    } else if (s.variableIsReadLocked(variableId)) {

			ArrayList<Integer> blockedBy = s.variableReadLockedBy(variableId);
			transaction.status = TransactionStatus.Waiting;

			for (int i : blockedBy) {
			    if (!transaction.waitingForTransactions.contains(i) && transaction.transactionId != i) {
				transaction.waitingForTransactions.add(i);
			    }
			}

			if (!waitingTransactions.contains(transaction)) {
			    waitingTransactions.add(transaction);
			}

		    }
		}
	    }
	}

	if (allSitesAcquired) {

	    if (shouldWait) {
		if (!waitingTransactions.contains(transaction)) {
		    transaction.status = TransactionStatus.Waiting;
		    transaction.waitingForTransactions.clear();

		    if (!waitingTransactions.contains(transaction)) {
			waitingTransactions.add(transaction);
		    }
		}
		return;
	    }

	    if (waitingTransactions.contains(transaction)) {
		waitingTransactions.remove(transaction);
	    }

	    transaction.status = TransactionStatus.Active;
	    transaction.waitingForTransactions.clear();
	    transaction.currentOperation = null;
	    return;

	} else {

	    if (!waitingTransactions.contains(transaction)) {
		transaction.status = TransactionStatus.Waiting;
		transaction.waitingForTransactions.clear();

		if (!waitingTransactions.contains(transaction)) {
		    waitingTransactions.add(transaction);
		}
	    }

	}

    }

    /**
     * Method for Fail Operation event of a given Site.
     * 
     * @param operation
     *            Operation with Fail instruction for a Site.
     * 
     * @author Jatin Malviya
     */
    void failOperation(Operation operation) {

	if (operation.site == Integer.MIN_VALUE) {

	    throw new IllegalStateException("Site ID missing in Fail Operation");
	}

	Site s = databaseManager.getSite(operation.site);
	s.fail(currentTime);

    }

    /**
     * Method for Recover Operation event of a given Site.
     * 
     * @param operation
     *            Operation with Recover instruction for a Site.
     * 
     * @author Jatin Malviya
     */
    void recoverOperation(Operation operation) {

	if (operation.site == Integer.MIN_VALUE) {

	    throw new IllegalStateException("Site ID missing in Recover Operation");
	}

	Site s = databaseManager.getSite(operation.site);
	s.recover();

    }

    /**
     * Method for End Operation for a given Transaction which causes the system
     * to report whether the Transaction can commit.
     * 
     * @param operation
     *            Operation with Recover instruction for a Transaction.
     * 
     * @author Jatin Malviya
     */
    void endOperation(Operation operation) {

	if (operation.transactionId == Integer.MIN_VALUE) {

	    throw new IllegalStateException("Transaction ID missing in End Operation");
	}

	Transaction transaction = transactions.get(operation.transactionId);

	if (transaction.status == TransactionStatus.Waiting && transaction.currentOperation != operation) {

	    throw new IllegalStateException(
		    "Transaction T" + transaction.transactionId + " is in Waiting state, Cannot run new Operation.");

	}

	if (transaction.status == TransactionStatus.Aborted) {

	    throw new IllegalStateException("Transaction T" + transaction.transactionId + " has already been aborted.");

	}

	if (transaction.status == TransactionStatus.Completed) {

	    throw new IllegalStateException(
		    "Transaction T" + transaction.transactionId + " has already been Committed.");

	}

	if (!transaction.isReadOnly) {

	    for (Site site : transaction.siteAcessTime.keySet()) {

		for (int accessTime : transaction.siteAcessTime.get(site)) {

		    if (accessTime < site.lastFailureTime || site.status == SiteStatus.Failed) {

			removeLocksTransaction(transaction);
			transaction.abort();
			System.out.println("Transaction T" + transaction.transactionId + " aborted at time " + currentTime + " because Site "
				+ site.siteId + " " + "failed after it was accessed.");
			return;

		    }

		}

	    }

	} else {
		
		for(Site site: transaction.siteAcessTime.keySet()) {
			
			if(site.status == SiteStatus.Failed) {
				
				transaction.status = TransactionStatus.Waiting;
				transaction.currentOperation = operation;
				transaction.waitingForTransactions.clear();

				if (!waitingTransactions.contains(transaction)) {
				    waitingTransactions.add(transaction);
				}
				return;
				
			}
			
		}
		
	}

	for (int varID : transaction.writelocks.keySet()) {

	    ArrayList<Integer> siteIds = transaction.writelocks.get(varID);
	    int valueToWrite = transaction.valuesWritten.get(varID);

	    for (int sid : siteIds) {

		Site s = databaseManager.getSite(sid);
		s.commitValue(transaction, varID, valueToWrite, currentTime);

	    }

	}

	removeLocksTransaction(transaction);
	transaction.commit();
	System.out.println("Transaction T" + transaction.transactionId + " committed" + " at time " + currentTime);

    }

    /**
     * Method to remove locks for a given transaction so that it can commit
     * after that.
     * 
     * @param transaction
     *            Transaction whose locks are needed to be removed.
     * 
     * @author Jatin Malviya
     */
    void removeLocksTransaction(Transaction transaction) {

	for (Site s : databaseManager.getSites()) {
	    s.removeLocks(transaction);
	}

	transaction.readlocks.clear();
	transaction.writelocks.clear();

    }

    /**
     * Method for Dump Operation which prints the committed values of all copies
     * of all variables at all sites.
     * 
     * @param operation
     *            Operation with Dump instruction.
     * 
     * @author Nishma Gorwara
     */
    void dumpOperation(Operation operation) {

	System.out.println("\nFull Dump at time " + currentTime);

	for (Site s : databaseManager.getSites()) {
	    s.printAllVariables();
	}
    }

    /**
     * Method for DumpI Operation which prints the committed values of all
     * copies of all variables at site i.
     * 
     * @param operation
     *            Operation with DumpI instruction.
     * 
     * @author Nishma Gorwara
     */
    void dumpIOperation(Operation operation) {

	if (operation.site == Integer.MIN_VALUE) {
	    throw new IllegalStateException("Site ID missing in DumpI Operation");
	}

	Site s = databaseManager.getSite(operation.site);

	s.printAllVariables();
    }

    /**
     * Method for DumpXj Operation which prints the committed values of all
     * copies of variable xj at all sites.
     * 
     * @param operation
     *            Operation with DumpXj instruction.
     * 
     * @author Nishma Gorwara
     */
    void dumpXjOperation(Operation operation) {

	if (operation.variable == Integer.MIN_VALUE) {
	    throw new IllegalStateException("Variable ID missing in DumpXj Operation");
	}

	System.out.println("\nDump for Variable x" + operation.variable + " at time " + currentTime);

	for (Site s : databaseManager.getSites()) {

	    if (s.hasVariable(operation.variable)) {

		s.printVariable(operation.variable);
	    }
	}

    }

    /**
     * Method to detect if the list of waiting transactions are in a deadlock.
     * 
     * @return List of waiting transactions if deadlock detected.
     * 
     * @author Jatin Malviya
     */
    public ArrayList<Transaction> detectCycleWait() {

	for (Transaction t : waitingTransactions) {

	    ArrayList<Transaction> temp = new ArrayList<Transaction>();
	    temp.add(t);

	    ArrayList<Transaction> result = detectNodesInCycle(t, temp);

	    if (result != null) {

		return result;
	    }

	}

	return null;
    }

    /**
     * Method to detect if the given transaction is in a wait-cycle of a
     * deadlock.
     * 
     * @param start
     *            Transaction denoting possible starting node of the cycle.
     * @param pathTillNow
     *            List of Transactions which are visited in the cycle.
     * @return List of Transactions which are present in the cycle.
     * 
     * @author Jatin Malviya
     */
    public ArrayList<Transaction> detectNodesInCycle(Transaction start, ArrayList<Transaction> pathTillNow) {

	ArrayList<Transaction> result;

	for (int i : start.waitingForTransactions) {

	    if (transactions.get(i).status != TransactionStatus.Active) {

		if (transactions.get(i) == pathTillNow.get(0)) {
		    return pathTillNow;
		}

		ArrayList<Transaction> temp = new ArrayList<Transaction>(pathTillNow);

		if (!temp.contains(transactions.get(i))) {
		    temp.add(transactions.get(i));
		    result = detectNodesInCycle(transactions.get(i), temp);

		    if (result != null) {
			return result;
		    }
		}

	    }

	}

	return null;
    }

}
