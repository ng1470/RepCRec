package nyu.cs.adb.repcrec;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;

/**
 * Class representing the required information for a Site where data is stored.
 * 
 * @author Jatin Malviya
 */
public class Site {

    int siteId;
    SiteStatus status;
    int lastFailureTime;
    TreeMap<Integer, Variable> variables;
    HashMap<Integer, ArrayList<Integer>> readLocks;
    HashMap<Integer, Integer> writeLocks;
    
    /**
     * Constructor to initialize the member variables for a Site given its id.
     * 
     * @param id
     *            Site id to set the other member variables.
     * 
     * @author Jatin Malviya
     */
    Site(int id) {

	siteId = id;
	status = SiteStatus.Active;
	lastFailureTime = 0;
	variables = new TreeMap<Integer, Variable>();
	readLocks = new HashMap<Integer, ArrayList<Integer>>();
	writeLocks = new HashMap<Integer, Integer>();

    }

    /**
     * Method to add a Variable and its value to the Site.
     * 
     * @param id
     *            Id of the given Variable.
     * @param value
     *            Value of the given Variable.
     * 
     * @author Jatin Malviya
     */
    public void addVariable(int id, int value) {

	Variable var = new Variable(id, value);
	variables.put(id, var);

    }

    /**
     * Method to check if the Site has a Variable when given its id.
     * 
     * @param id
     *            Variable id to check if present on Site.
     * 
     * @return true if Site has Variable else false.
     * 
     * @author Jatin Malviya
     */
    public boolean hasVariable(int id) {
	return variables.containsKey(id);
    }

    /**
     * Method to check the Variable on Site is write locked when given its id.
     * 
     * @param id
     *            Variable id to check if it is write locked.
     * 
     * @return true if Variable is write locked else false.
     * 
     * @author Jatin Malviya
     */
    public boolean variableIsWriteLocked(int id) {
	return writeLocks.containsKey(id);
    }

    /**
     * Method to check the Variable on Site is read locked when given its id.
     * 
     * @param id
     *            Variable id to check if it is read locked.
     * 
     * @return true if Variable is read locked else false.
     * 
     * @author Jatin Malviya
     */
    public boolean variableIsReadLocked(int id) {
	return readLocks.containsKey(id);
    }

    /**
     * Method to get the Transaction on Site which write locked a given
     * Variable.
     * 
     * @param id
     *            Variable id which is write locked.
     * 
     * @return Transaction which is write locking a given Variable.
     * 
     * @author Jatin Malviya
     */
    public int variableWriteLockedBy(int id) {

	if (!variableIsWriteLocked(id)) {
	    throw new IllegalStateException("Variable x" + id + " is not write locked.");
	}

	return writeLocks.get(id);
    }

    /**
     * Method to get the Transaction on Site which read locked a given Variable.
     * 
     * @param id
     *            Variable id which is read locked.
     * 
     * @return Transaction which is read locking a given Variable.
     * 
     * @author Jatin Malviya
     */
    public ArrayList<Integer> variableReadLockedBy(int id) {

	if (!variableIsReadLocked(id)) {
	    throw new IllegalStateException("Variable x" + id + " is not read locked.");
	}

	return readLocks.get(id);
    }

    /**
     * Method to check if a Variable on Site can be read given its id and
     * Transaction.
     * 
     * @param transaction
     *            Transaction which may be write locking a given Variable.
     * @param id
     *            Variable id to check if it can be read.
     * 
     * @return true if the Variable can be read else false.
     * 
     * @author Jatin Malviya
     */
    public boolean canReadVariable(Transaction transaction, int id) {

	if (!variables.containsKey(id) || status == SiteStatus.Failed) {
	    return false;
	}

	Variable var = variables.get(id);

	if (status == SiteStatus.Recovering && var.isReplicated == true && var.isStale == true) {
	    return false;
	}

	if (variableIsWriteLocked(id)) {

	    if (transaction.transactionId == variableWriteLockedBy(id)) {
		return true;
	    } else {
		return false;
	    }

	}

	return true;

    }

    /**
     * Method to check if a Read-only Variable on Site can be read given its id
     * and Transaction.
     * 
     * @param id
     *            Read-only Variable id to check if it can be read.
     * 
     * @return true if the Read-only Variable can be read else false.
     * 
     * @author Jatin Malviya
     */
    public boolean canReadROVariable(Transaction transaction, int id) {

	if (!variables.containsKey(id) || status == SiteStatus.Failed) {
	    return false;
	}

	Variable var = variables.get(id);

	if(!transaction.activeStartSites.contains(siteId)) {
		return false;
	}
	/*if (status == SiteStatus.Recovering && var.isReplicated == true && var.isStale == true) {
	    return false;
	}*/

	return true;
    }

    /**
     * Method to check if a Variable on Site can be written given its id and
     * Transaction.
     * 
     * @param transaction
     *            Transaction which may be read/write locking a given Variable.
     * @param id
     *            Variable id to check if it can be written.
     * 
     * @return true if the Variable can be written else false.
     * 
     * @author Jatin Malviya
     */
    public boolean canWriteVariable(Transaction transaction, int id) {

	if (!variables.containsKey(id) || status == SiteStatus.Failed) {
	    return false;
	}

	if (variableIsWriteLocked(id)) {

	    if (transaction.transactionId == variableWriteLockedBy(id)) {
		return true;
	    } else {
		return false;
	    }

	}

	if (variableIsReadLocked(id)) {

	    ArrayList<Integer> locks = variableReadLockedBy(id);

	    if (locks.size() == 1 && transaction.transactionId == locks.get(0)) {
		return true;
	    } else {
		return false;
	    }

	}

	return true;

    }

    /**
     * Method to put Read Lock on a given Variable for a Transation if not
     * already exists.
     * 
     * @param transaction
     *            Transaction of the given Variable.
     * @param id
     *            Variable id to put a Read Lock on.
     * 
     * @author Jatin Malviya
     */
    public void putReadLock(Transaction transaction, int id) {

	if (!canReadVariable(transaction, id)) {

	    throw new IllegalStateException("Variable x" + id + " cannot be read locked.");

	}

	ArrayList<Integer> locks = null;

	if (readLocks.containsKey(id)) {
	    locks = variableReadLockedBy(id);
	} else {
	    locks = new ArrayList<Integer>();
	}

	if (!locks.contains(transaction.transactionId)) {

	    locks.add(transaction.transactionId);
	    readLocks.put(id, locks);

	}

    }

    /**
     * Method to put Write Lock on a given Variable for a Transation if not
     * already exists.
     * 
     * @param transaction
     *            Transaction of the given Variable.
     * @param id
     *            Variable id to put a Write Lock on.
     * 
     * @author Jatin Malviya
     */
    public void putWriteLock(Transaction transaction, int id) {

	if (!canWriteVariable(transaction, id)) {
	    throw new IllegalStateException("Variable x" + id + " cannot be write locked.");
	}

	readLocks.remove((Integer) id);

	writeLocks.put(id, transaction.transactionId);

    }

    /**
     * Method to get value of a Read-only Variable at a particular time for a
     * given Transaction.
     * 
     * 
     * @param transaction
     *            Transaction of reading the given Read-only Variable.
     * @param id
     *            Read-only Variable id whose value is read.
     * @param timestamp
     *            Timestamp at which Transaction occurs.
     * 
     * @return Value of the Read-only Variable of given Transaction and time.
     * 
     * @author Jatin Malviya
     */
    public int readRO(Transaction transaction, int id, int timestamp) {

	if (!canReadROVariable(transaction, id)) {
	    throw new IllegalStateException("Variable x" + id + " cannot be read.");
	}

	Variable var = variables.get(id);

	return var.getValue(timestamp);

    }

    /**
     * Method to check if the Transaction has Read Lock on the given Variable.
     * 
     * @param transaction
     *            Transaction of the given Variable.
     * @param id
     *            Variable id which may be read locked.
     * 
     * @return true if Transaction has Read Lock on the Variable else false.
     * 
     * @author Jatin Malviya
     */
    public boolean transactionHasReadLock(Transaction transaction, int id) {

	if (!variables.containsKey(id) || status == SiteStatus.Failed) {
	    return false;
	}

	if (!readLocks.containsKey(id)) {
	    return false;
	}

	return readLocks.get(id).contains(transaction.transactionId);
    }

    /**
     * Method to check if the Transaction has Write Lock on the given Variable.
     * 
     * @param transaction
     *            Transaction of the given Variable.
     * @param id
     *            Variable id which may be write locked.
     * 
     * @return true if Transaction has Write Lock on the Variable else false.
     * 
     * @author Jatin Malviya
     */
    public boolean transactionHasWriteLock(Transaction transaction, int id) {

	if (!variables.containsKey(id) || status == SiteStatus.Failed) {
	    return false;
	}

	if (!writeLocks.containsKey(id)) {
	    return false;
	}

	return (writeLocks.get(id) == transaction.transactionId);
    }

    /**
     * Method to get value of a Variable for a given Transaction.
     * 
     * @param transaction
     *            Transaction of reading the given Read-only Variable.
     * @param id
     *            Read-only Variable id whose value is read.
     * 
     * @return Value of the Variable of given Transaction.
     * 
     * @author Jatin Malviya
     */
    public int read(Transaction transaction, int id) {

	if (!transactionHasReadLock(transaction, id)) {
	    throw new IllegalStateException("Variable x" + id + " is not read locked by T" + transaction.transactionId);
	}

	return variables.get(id).getValue();

    }

    /**
     * Method to commit value of a Variable for a given Transaction and time.
     * 
     * @param transaction
     *            Transaction of the Variable.
     * @param id
     *            Variable id whose value is commited.
     * @param val
     *            Commit value of the given variable.
     * @param timestamp
     *            Time of the given Transaction.
     * 
     * @author Jatin Malviya
     */
    public void commitValue(Transaction transaction, int id, int val, int timestamp) {

	if (!transactionHasWriteLock(transaction, id)) {
	    throw new IllegalStateException(
		    "Variable x" + id + " is not write locked by T" + transaction.transactionId);
	}

	Variable var = variables.get(id);

	var.writeVariable(val, timestamp);

	if (status == SiteStatus.Recovering && var.isStale) {
	    var.isStale = false;
	}

    }

    /**
     * Method to call when the Site fails.
     * 
     * @param timestamp
     *            Time at which the Site fails.
     * 
     * @author Jatin Malviya
     */
    public void fail(int timeStamp) {

	status = SiteStatus.Failed;
	readLocks.clear();
	writeLocks.clear();
	lastFailureTime = timeStamp;

    }

    /**
     * Method to call when the Site recovers and the status(stalenaess) of all
     * the Variables on Site is updated.
     * 
     * @author Jatin Malviya
     */
    public void recover() {

	status = SiteStatus.Recovering;

	for (Variable var : variables.values()) {

	    if (var.isReplicated) {
		var.isStale = true;
	    } else {
		var.isStale = false;
	    }

	}
    }

    /**
     * Method to remove Read or Write Locks of a given Transaction.
     * 
     * @param transaction
     *            Transaction whose locks are removed.
     * 
     * @author Jatin Malviya
     */
    public void removeLocks(Transaction transaction) {

	// Remove Read Locks for a given Transaction
	ArrayList<Integer> rlocks = new ArrayList<Integer>(readLocks.keySet());

	for (int rlock : rlocks) {

	    ArrayList<Integer> arr = readLocks.get(rlock);

	    if (arr.contains(transaction.transactionId)) {

		arr.remove((Integer) transaction.transactionId);
		if (arr.isEmpty()) {
		    readLocks.remove(rlock);
		} else {
		    readLocks.put(rlock, arr);
		}

	    }

	}

	// Remove Write Locks for a given Transaction
	ArrayList<Integer> wlocks = new ArrayList<Integer>(writeLocks.keySet());

	for (int wlock : wlocks) {

	    if (writeLocks.get(wlock) == transaction.transactionId) {
		writeLocks.remove((Integer) wlock);
	    }

	}

    }

    /**
     * Method to print values of a given Variable at the Site.
     * 
     * @param varId
     *            Id of the Variable on Site whose value is printed.
     * 
     * @author Jatin Malviya
     */
    void printVariable(int varId) {

	if (!variables.containsKey(varId)) {
	    throw new IllegalStateException("Variable not found at Site" + siteId);
	}

	System.out.println("x" + varId + "." + siteId + " = " + variables.get(varId).value);

    }

    /**
     * Method to print values of all the Variables at the Site.
     * 
     * @author Jatin Malviya
     */
    void printAllVariables() {

	System.out.println("\nDump for Site " + siteId);

	for (int varId : variables.keySet()) {
	    printVariable(varId);
	}

    }

}
