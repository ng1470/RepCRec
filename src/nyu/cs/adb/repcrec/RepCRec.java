package nyu.cs.adb.repcrec;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

/**
 * Main class for Distributed replicated concurrency control and recovery. We
 * have implemented a distributed database, complete with multiversion
 * concurrency control, deadlock detection, replication, and failure recovery.
 * 
 * @author Jatin Malviya (jm6474@nyu.edu)
 * @author Nishma Gorwara (ng1470@nyu.edu)
 *
 */
public class RepCRec {

    /**
     * Main method to process transactions by taking input instructions from a
     * file or the standard input, output goes to standard out.
     * 
     * @param args
     * @throws FileNotFoundException
     * 
     * @author Nishma Gorwara
     */
    public static void main(String[] args) throws FileNotFoundException {

	RepCRec rcr = new RepCRec();
	Scanner scannerInput;

	// Checks if one input argument present,
	// then read from file else read from console
	if (args.length == 1) {

	    FileReader fr = new FileReader(args[0]);
	    scannerInput = new Scanner(fr);

	} else {

	    System.out.println("Please enter input to read from console(Enter quit to exit):");
	    scannerInput = new Scanner(System.in);

	}

	rcr.parseInput(scannerInput);
	scannerInput.close();
    }

    /**
     * Function to parse the given input instructions and create appropriate
     * operations based on the operation type.
     * 
     * @param in
     *            Scanner input from file or console
     * 
     * @author Nishma Gorwara
     */
    private void parseInput(Scanner in) {

	// Creating a Transaction Manager
	TransactionManager TM = new TransactionManager();
	int time = 0;

	while (in.hasNext()) {

	    // Reading an instruction step
	    String opLine = in.nextLine().trim();
	    if (opLine.startsWith("//") || opLine.isEmpty()) {
		continue;
	    } else if (opLine.startsWith("=") || opLine.contains("quit")) {
		break;
	    }
	    time++;

	    // Processing concurrent operations
	    String[] operations = opLine.split(";");
	    for (int i = 0; i < operations.length; i++) {

		// Processing an Operation object
		String opString = operations[i].trim();
		if (!opString.isEmpty()) {

		    Operation op = new Operation();
		    String opInstruction = opString.substring(0, opString.indexOf("(")).trim();
		    String opInputs = opString.substring(opString.indexOf("(") + 1, opString.indexOf(")")).trim();

		    // Matching operation type and setting member variables for
		    // the Operation object
		    if (opInstruction.equalsIgnoreCase("begin")) {

			op.type = OperationType.Begin;
			op.transactionId = Integer.parseInt(opInputs.substring(1));

		    } else if (opInstruction.equalsIgnoreCase("beginro")) {

			op.type = OperationType.BeginRO;
			op.transactionId = Integer.parseInt(opInputs.substring(1));

		    } else if (opInstruction.equalsIgnoreCase("r")) {

			op.type = OperationType.Read;
			String tid = opInputs.split(",")[0].trim();
			String var = opInputs.split(",")[1].trim();
			op.transactionId = Integer.parseInt(tid.substring(1));
			op.variable = Integer.parseInt(var.substring(1));

		    } else if (opInstruction.equalsIgnoreCase("w")) {

			op.type = OperationType.Write;
			String tid = opInputs.split(",")[0].trim();
			String var = opInputs.split(",")[1].trim();
			String val = opInputs.split(",")[2].trim();
			op.transactionId = Integer.parseInt(tid.substring(1));
			op.variable = Integer.parseInt(var.substring(1));
			op.value = Integer.parseInt(val);

		    } else if (opInstruction.equalsIgnoreCase("dump")) {

			if (opInputs.isEmpty()) {

			    op.type = OperationType.Dump;

			} else if (opInputs.startsWith("x")) {

			    op.type = OperationType.DumpXj;
			    op.variable = Integer.parseInt(opInputs.substring(1));

			} else {

			    op.type = OperationType.DumpI;
			    op.site = Integer.parseInt(opInputs);

			}

		    } else if (opInstruction.equalsIgnoreCase("end")) {

			op.type = OperationType.End;
			op.transactionId = Integer.parseInt(opInputs.substring(1));

		    } else if (opInstruction.equalsIgnoreCase("fail")) {

			op.type = OperationType.Fail;
			op.site = Integer.parseInt(opInputs);

		    } else if (opInstruction.equalsIgnoreCase("recover")) {

			op.type = OperationType.Recover;
			op.site = Integer.parseInt(opInputs);

		    }

		    // Passing the Operation object to the Transaction Manager
		    TM.tick(op, time);

		}

	    }

	}

    }

}