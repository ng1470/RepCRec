package nyu.cs.adb.repcrec;

/**
 * Enum to represent different possible Status of a Transaction.
 * 
 * @author Nishma Gorwara
 */
public enum TransactionStatus {

    Active, Waiting, Aborted, Completed

}
