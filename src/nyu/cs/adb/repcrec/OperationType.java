package nyu.cs.adb.repcrec;

/**
 * Enum to repesent different operation types.
 * 
 * @author Nishma Gorwara
 */
public enum OperationType {

    Begin, BeginRO, Read, Write, Dump, DumpI, DumpXj, Fail, Recover, End

}
