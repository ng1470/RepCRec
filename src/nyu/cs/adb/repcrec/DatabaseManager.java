package nyu.cs.adb.repcrec;

import java.util.ArrayList;
import java.util.TreeMap;

/**
 * Class to set up the initial database and manage the database sites.
 * 
 * @author Nishma Gorwara
 */
public class DatabaseManager {

    private TreeMap<Integer, Site> sites;

    /**
     * Constructor to initialize the Database Manager.
     * 
     */
    DatabaseManager() {
	sites = new TreeMap<Integer, Site>();
    }

    /**
     * Method to initialize the database sites and their variables.
     * 
     * @author Nishma Gorwara
     */
    public void initialize() {

	// Initializing 10 sites numbered 1 to 10.
	for (int i = 1; i <= 10; i++) {

	    Site s = new Site(i);
	    sites.put(i, s);
	}

	// Each variable xi is initialized to the value 10i.
	for (int i = 1; i <= 20; i++) {

	    if (i % 2 == 0) {
		// The even indexed variables are at all sites.
		for (int j = 1; j <= 10; j++) {
		    Site s = sites.get(j);
		    s.addVariable(i, 10 * i);
		}

	    } else {

		// The odd indexed variables are at one site each.
		// (i.e. 1 + index number mod 10 )
		Site s = sites.get(1 + (i % 10));
		s.addVariable(i, 10 * i);

	    }

	}

    }

    /**
     * Method to get a site given an id.
     * 
     * @param id
     *            Id to find the corresponding site.
     * @return Site given its id.
     * 
     * @author Nishma Gorwara
     */
    public Site getSite(int id) {

	if (!sites.containsKey(id)) {
	    throw new IllegalStateException("No Site found with id" + id);
	}

	return sites.get(id);

    }

    /**
     * Method to get all the sites.
     * 
     * @return Sites in the database.
     * 
     * @author Nishma Gorwara
     */
    public ArrayList<Site> getSites() {
	return new ArrayList<Site>(sites.values());
    }

    /**
     * Method to set all the sites in the map
     * 
     * @param s
     *            Treemap representing all the sites in the database.
     * 
     * @author Nishma Gorwara
     */
    public void setSites(TreeMap<Integer, Site> s) {
	sites = s;
    }

}
