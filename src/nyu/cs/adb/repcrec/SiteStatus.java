package nyu.cs.adb.repcrec;

/**
 * Enum to represent different possible Status of a Site.
 * 
 * @author Jatin Malviya
 */
public enum SiteStatus {

    Active, Failed, Recovering

}
